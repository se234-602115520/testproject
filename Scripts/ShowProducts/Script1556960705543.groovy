import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFctory
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.By as By
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://3.82.158.88:8085/')

WebUI.setText(findTestObject('ShowProducts/input_username'), 'user')

WebUI.setEncryptedText(findTestObject('ShowProducts/input_password'), '1/VWEm4uipk=')

WebUI.click(findTestObject('ShowProducts/button_Login'))

WebUI.verifyElementText(findTestObject('ShowProducts/h5_Garden'), 'Garden')

WebUI.verifyElementText(findTestObject('ShowProducts/h5_Banana'), 'Banana')

WebUI.verifyElementText(findTestObject('ShowProducts/h5_Orange'), 'Orange')

WebUI.verifyElementText(findTestObject('ShowProducts/h5_Papaya'), 'Papaya')

WebUI.verifyElementText(findTestObject('ShowProducts/h5_Rambutan'), 'Rambutan')

WebDriver driver = DriverFctory.getWebDriver()

WebElement Table = driver.findElement(By.xpath('/html/body/app-root/app-product-list/div/div[2]/div'))

'To local rows of table it will Capture all the rows available in the table '
List<WebElement> Rows = Table.findElements(By.className('farmer-card'))

println('No. of rows: ' + Rows.size())

WebUI.verifyEqual(Rows.size(),5)

WebUI.closeBrowser()

