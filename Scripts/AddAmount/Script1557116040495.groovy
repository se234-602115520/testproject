import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFctory
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.By as By
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://3.82.158.88:8085/')

WebUI.setText(findTestObject('AddAmountWithPrice/input_Username_username'), username)

WebUI.setText(findTestObject('AddAmountWithPrice/input_Password_password'), password)

WebUI.click(findTestObject('AddAmountWithPrice/button_Login'))

WebUI.click(findTestObject('AddAmountWithPrice/button_add garden to cart'))

WebUI.click(findTestObject('AddAmountWithPrice/button_add banana to cart'))

WebUI.click(findTestObject('AddAmountWithPrice/button_add orange to cart'))

WebUI.click(findTestObject('AddAmountWithPrice/button_add papaya to cart'))

WebUI.click(findTestObject('AddAmountWithPrice/button_add rambutan to cart'))

WebUI.click(findTestObject('AddAmountWithPrice/a_Carts            5'))

WebUI.verifyElementText(findTestObject('AddAmountWithPrice/span_5'), '5')

WebUI.setText(findTestObject('AddAmountWithPrice/input_Garden_amount'), garden_amount)

WebUI.setText(findTestObject('AddAmountWithPrice/input_Banana_amount'), banana_amount)

WebUI.setText(findTestObject('AddAmountWithPrice/input_Orange_amount'), orange_amount)

WebUI.setText(findTestObject('AddAmountWithPrice/input_Papaya_amount'), papaya_amount)

WebUI.setText(findTestObject('AddAmountWithPrice/input_Rambutan_amount'), rambutan_amount)

WebUI.verifyElementText(findTestObject('AddAmountWithPrice/p_Total price'), total_price)

WebDriver driver = DriverFctory.getWebDriver()

WebElement Table = driver.findElement(By.xpath('//*[@id="add-row"]/div/table/tbody'))

'To local rows of table it will Capture all the rows available in the table '
List<WebElement> Rows = Table.findElements(By.className('text-right'))

println('No. of rows: ' + Rows.size())

WebUI.verifyEqual(Rows.size(), 5)

WebUI.setText(findTestObject('AddAmountWithPrice/input_Garden_amount'), garden_amount2)

WebUI.setText(findTestObject('AddAmountWithPrice/input_Banana_amount'), banana_amount2)

WebUI.setText(findTestObject('AddAmountWithPrice/input_Orange_amount'), orange_amount2)

WebUI.setText(findTestObject('AddAmountWithPrice/input_Papaya_amount'), papaya_amount2)

WebUI.setText(findTestObject('AddAmountWithPrice/input_Rambutan_amount'), rambutan_amount2)

WebUI.verifyElementText(findTestObject('AddAmountWithPrice/p_Total price'), total_price2)

WebDriver driver2 = DriverFctory.getWebDriver()

WebElement Table2 = driver2.findElement(By.xpath('//*[@id="add-row"]/div/table/tbody'))

'To local rows of table it will Capture all the rows available in the table '
List<WebElement> Rows2 = Table2.findElements(By.className('text-right'))

println('No. of rows: ' + Rows2.size())

WebUI.verifyEqual(Rows2.size(), 5)

WebUI.closeBrowser()

