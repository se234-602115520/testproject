import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://3.82.158.88:8085/')

WebUI.setText(findTestObject('AddProduct/input_Username_username'), 'user')

WebUI.setText(findTestObject('AddProduct/input_Password_password'), 'user')

WebUI.click(findTestObject('AddProduct/button_Login'))

WebUI.click(findTestObject('AddProduct/button_add to cart'))

WebUI.verifyElementNotClickable(findTestObject('AddProduct/button_add to cart'))

WebUI.click(findTestObject('AddProduct/a_Carts            1'))

WebUI.verifyElementText(findTestObject('AddProduct/td_150 THB'), '150 THB')

WebUI.verifyElementText(findTestObject('AddProduct/td_Banana'), 'Banana')

WebUI.verifyElementText(findTestObject('AddProduct/p_Total price  150 THB'), 'Total price: 150 THB')

WebUI.closeBrowser()

