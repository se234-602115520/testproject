<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>LoginFailedSuite1</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>4b7de82b-27ab-4e91-ae5f-7fcbb4a0dbef</testSuiteGuid>
   <testCaseLink>
      <guid>5e6158ae-1f44-485f-b827-ded5fb021c56</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/LoginFailed1</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>3f6e1fa7-8552-4d3f-b9a8-97087b440909</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/LoginFailed1Data</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>3f6e1fa7-8552-4d3f-b9a8-97087b440909</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>0880ebdd-7c07-434c-98d5-54bda858e903</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>3f6e1fa7-8552-4d3f-b9a8-97087b440909</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>c397a6fd-a1e2-47c0-b732-ecd8fdc9ad62</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>3f6e1fa7-8552-4d3f-b9a8-97087b440909</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>showbar</value>
         <variableId>bb661d18-ee66-448f-b0a6-3cf0732fc321</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
