<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>LoginFailedSuite2</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>f21cc279-3c6d-4f10-9372-993342bb3830</testSuiteGuid>
   <testCaseLink>
      <guid>66781c8f-acec-421c-9157-fc8a1ed0fbe2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/LoginFailed2</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>8b8dadc6-5de7-4ec7-b104-874fb4c92f1e</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/LoginFailed2Data</testDataId>
      </testDataLink>
      <variableLink>
         <testDataLinkId>8b8dadc6-5de7-4ec7-b104-874fb4c92f1e</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>3bc322d6-8f5f-4701-b592-74f5f9a80dd1</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>8b8dadc6-5de7-4ec7-b104-874fb4c92f1e</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>showbar</value>
         <variableId>0ddb5722-2c5c-4846-8810-ddc183c796e5</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
